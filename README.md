# Socket Server Logger Nodejs

Reads data from a FIFO buffer, generates some statistics (available at HTTP server)
and is able to send the retrieved data (json) to external service

# Install

    sudo apt-get install build-essential pkg-config libsystemd-dev

# Run

Just `node index.js`

# Configuration

There is an example configuration file `config.dist.json`, so rename it to config.json and configure as required

`delaySeconds`: Time betwen each push of new items to the statistics (default: `60`[s])

`forgetStatistics`: How long to keep the statistics in memory (default: `24`[h], `"<number><h|d|m>"` for hour, day or month respectively)

`http`: Object with single key "port" with the port number to run the HTTP server on (default: `8080`)

# Restarting service

`systemctl restart socket-server-logger.service`

If you want then to check if the service is running - run `systemctl status socket-server-logger.service`
