import { Dimension } from "aws-sdk/clients/cloudwatch";
import { CredentialsOptions } from "aws-sdk/lib/credentials";

export default interface Config {
  delaySeconds?: number
  fifo?: {path: string}
  socket?: {path: string}
  forgetStatistics?: number
  http?: {port: number}
  allowedIps?: string[]
  aws?: {
    credentials?: CredentialsOptions,
    region?: string,
    cloudWatch?: {
      namespace: string,
      pushSeconds?: number,
      dimensions?: Dimension[]
    }
  },
  logViewer?: {
    url: string,
    authorizations: {
      [key: string]: string,
    },
  }
}