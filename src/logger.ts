import process from 'process'

const { name } = require('../package.json');
const Journald = require( 'systemd-journald' );
const log = new Journald({syslog_identifier: name});

export default class Logger {
  static info(...args: any): void {
    if (process.env.STARTED_BY_SYSTEMD === 'yes') {
      log.info(...args)
    } else {
      console.log(...args)
    }
  }
  static warn(...args: any): void {
    if (process.env.STARTED_BY_SYSTEMD === 'yes') {
      log.warning(...args)
    } else {
      console.warn(...args)
    }
  }
  static err(...args: any): void {
    if (process.env.STARTED_BY_SYSTEMD === 'yes') {
      log.err(...args)
    } else {
      console.error(...args)
    }
  }
}