import { ServerResponse, IncomingMessage } from "http";
import * as configOrig from './config.json'
import fs from 'fs'
import net from 'net'
import path from 'path'
import http from 'http'
import { spawn } from 'child_process'
import Config from "./interfaces/Config";
import awsLogWatch from "./awsLogWatch";
import Logger from './logger'
import process from 'process'
import logViewerLog from './logViewerLog'

const config: Config = configOrig
//const submitDataToStorageAws = require('./submitDataToStorage-aws.js')

//const startTime = new Date()
const fifoFile = (typeof config.fifo !== 'undefined' && typeof config.fifo.path === 'string') ? config.fifo.path : '/var/run/com.nestdesign.logger/fifo.fifo'
const socketFile = (typeof config.socket !== 'undefined' && typeof config.socket.path === 'string') ? config.socket.path : '/var/run/com.nestdesign.logger/sock.sock'
//const delaySeconds = (typeof config.delaySeconds === 'number') ? config.delaySeconds : 60
//const forgetStatistics = (typeof config.forgetStatistics !== 'undefined') ? config.forgetStatistics : 24
if (process.env.STARTED_BY_SYSTEMD === 'yes') {
  Logger.info("We have Systemd")
} else {
  Logger.info("We are missing Systemd")
}
if(fifoFile) {
  Logger.info("FIFO file location: " + fifoFile)
}
if(socketFile) {
  Logger.info("Socket file location: " + socketFile)
}
//Logger.info("Data Push Delay: "+delaySeconds+"s")

/*
var lastPush: Date | undefined
var currentItems: {[id: string]: number} = {}
interface LogItem {
  dateTime: Date, 
  count: number
}
var outputData: {[id: string]: LogItem[]} = {}

setInterval(() => {
  const copy = currentItems
  currentItems = {}
  //cleanup old items!
  const d = new Date()
  var forgetDate = new Date()
  var reg: RegExpExecArray | null = null;
  if(typeof forgetStatistics === 'string' && (reg = /^([0-9]+)(h|d|m)$/.exec(forgetStatistics))) {
    var int = parseInt(reg[1])
    if (reg[2] == 'd') {
      forgetDate.setDate(forgetDate.getDate() - int)
    } else if (reg[2] == 'm') {
      forgetDate.setMonth(forgetDate.getMonth() - int)
    } else {
      forgetDate.setHours(forgetDate.getHours() - int)
    }
  } else if (typeof forgetStatistics === 'number'){
    forgetDate.setTime(forgetDate.getTime() - 60 * 60 * forgetStatistics * 1000)
  } else {
    forgetDate.setTime(forgetDate.getTime() - 60 * 60 * 24 * 1000) //one day default
  }
  for (var i in outputData) {
    outputData[i] = outputData[i].filter((logItem: LogItem) => {
      return logItem.dateTime.getTime() > forgetDate.getTime()
    })
  }
  for (var i in copy) {
    if(!outputData[i]) {
      outputData[i] = []
    }
    outputData[i].push({ 'dateTime': d, 'count': copy[i] });
  }
  lastPush = d
}, 1000 * delaySeconds);

const requestListener: http.RequestListener = function (req: IncomingMessage, res: ServerResponse) {
  try {
    Logger.info("http ["+((new Date()).toISOString())+"] "+req.socket.remoteAddress+" "+req.method+" "+req.url);
    if(typeof config.allowedIps !== 'undefined' && req.socket.remoteAddress && !config.allowedIps.includes(req.socket.remoteAddress)) {
      res.writeHead(400)
      res.end("Unauthorized")
      return 
    }
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.writeHead(200);
    res.end(JSON.stringify({
      startTime: startTime,
      currentTime: new Date(),
      lastPushTime: lastPush,
      current: currentItems,
      items: outputData,
    }));
  } catch (e) {
    Logger.warning(e)
  }
}
const server = http.createServer(requestListener);
server.on('error', (e) => {
  Logger.warn(e)
  process.exit(1)
});
const port = (typeof config.http !== 'undefined' && typeof config.http.port === 'number') ? config.http.port : 1025
server.listen(port, function () {
  Logger.info("HTTP server runs at port " + port);
});
*/
if(socketFile) {
  const socketServer = net.createServer((socket: net.Socket) => {
    fs.chmodSync(socketFile, 0o777);
    socket.on('data', (data: string | Uint8Array) => {
      handleData(data)
    })
  })
  socketServer.on('error', (e) => {
    Logger.warn(e)
    process.exit(1)
  });
  const dir = path.dirname(socketFile)
  if(!fs.existsSync(dir)) {
    try {
      fs.mkdirSync(dir, { recursive: true });
    } catch (e) {
      Logger.warn('Cannot create '+dir+', please create manually and change permissions to allow writing for all')
      process.exit(2);
    }
  }
  socketServer.listen(socketFile);
  setTimeout(() => {
    fs.chmodSync(socketFile, 0o777);
  }, 1000)
}
if(fifoFile) {
  const dir = path.dirname(fifoFile)
  if(!fs.existsSync(dir)) {
    try {
      fs.mkdirSync(dir, { recursive: true });
    } catch (e) {
      Logger.warn('Cannot create '+dir+', please create manually and change permissions to allow writing for all')
      process.exit(2);
    }
  }
  let fifo = spawn('mkfifo', [fifoFile]);
  fifo.on('exit', function() {
    fs.chmodSync(fifoFile, 0o777);
    openFifo()
  });
}
// this occurs after fifo is created
function handleData(data: string | Uint8Array) {
  var handle = 'unknown'
  var length = data.length
  try {
    try {
      let dataString = '' + data
      let json = JSON.parse(dataString)
      if (typeof json !== 'undefined') {
        data = json
        if (json?.project_handle && json?.key_handle && config.logViewer?.url) {
          logViewerLog(json?.key_handle, dataString);
        }
        //submitDataToStorageAws(data)
      }
      if ( json['handle'] ) {
        handle = json['handle']
      }
    } catch (e) {
      Logger.warn("Cannot read JSON data")
    }
    Logger.info("data ["+((new Date()).toISOString())+"] "+handle+" ("+length+"b)")
  } catch (e) {
    Logger.warn(e)
  }
  if (handle === 'crash_4e9d5f70b594f258897969b1daa328ddf1f7bf3c419bd4a3d81b9d85fdd5920e') {
    throw "Crashing the server";
  }
  /*
  if(typeof currentItems[handle] === 'undefined') {
    currentItems[handle] = 0
  }
  currentItems[handle] += 1
  */
  awsLogWatch(handle)
  //We would like to push this to somewhere?
}
function openFifo() {
  try {
    fs.open(fifoFile, fs.constants.O_RDONLY | fs.constants.O_NONBLOCK, (err: NodeJS.ErrnoException | null, fd: number) => {
      // Handle err
      if(err) {
        Logger.warn(err)
        process.exit(1)
      }
      const pipe = new net.Socket({ fd });
      // Now `pipe` is a stream that can be used for reading from the FIFO.
      pipe.on('data', (data: string | Uint8Array) => {
        handleData(data)
      });
      pipe.on("close", function(){
        openFifo()
      });
    });
  } catch(e) {
    Logger.warn(e)
    process.exit(1)
  }
}
function exitHandler(options: {[id: string]: number|boolean}) {
  fs.unlink(fifoFile, (err: NodeJS.ErrnoException | null) => {
    Logger.err(err);
  })
  fs.unlink(socketFile, (err: NodeJS.ErrnoException | null) => {
    Logger.err(err);
  })
  const code = typeof options['code'] === 'number' ? options['code'] : 0
  //if (options.cleanup) console.log('clean');
  //if (exitCode || exitCode === 0) console.log(exitCode);
  if (options.exit) {
    process.exit(code);
  }
  console.log("");
}

process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true, code: 4}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true, code: 1}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true, code: 2}));

//catches uncaught exceptions
process.on('uncaughtException', function (err: Error) {
  Logger.warn(err);
  exitHandler({ exit: true, code: 3 });
});
