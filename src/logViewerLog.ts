import * as configOrig from './config.json'
import Config from './interfaces/Config';
import Logger from './logger'
import Axios from 'axios'

const config: Config = configOrig

export default async function logViewerLog(keyHandle: string, data: string) {
  if (!config.logViewer?.url || !config.logViewer.authorizations || !config.logViewer.authorizations[keyHandle]) {
    Logger.warn("logViewer [" + ((new Date()).toISOString()) + "] missing Log Viewer settings for " + keyHandle)
    return
  }

  const options = {
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(data),
      'Authorization': 'Bearer ' + config.logViewer.authorizations[keyHandle]
    },
  };

  try {
    const response = await Axios.post(config.logViewer.url, data, options);
    if (response.data.result === 'success') {
      Logger.info("logViewer [" + ((new Date()).toISOString()) + "] successfully saved log: " + response.data.message)
    } else {
      Logger.err("logViewer [" + ((new Date()).toISOString()) + "] could not save log: " + JSON.stringify(response.data))
    }
  } catch (error) {
    if (error instanceof Axios.AxiosError) {
      if (error.response?.data) {
        Logger.err("logViewer [" + ((new Date()).toISOString()) + "] request error: " + (error.response?.data.message ? error.response?.data.message : JSON.stringify(error.response?.data)))
      } else {
        Logger.err("logViewer [" + ((new Date()).toISOString()) + "] request error: " + error.message)
      }
    } else if (error instanceof Error) {
      Logger.err("logViewer [" + ((new Date()).toISOString()) + "] request error: " + error.message)
    } else {
      Logger.err("logViewer [" + ((new Date()).toISOString()) + "] request error: unknown error")
    }
  }
}