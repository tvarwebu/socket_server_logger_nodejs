var config = require('./config.json')
import AWS from 'aws-sdk'
import { WriteRequests } from 'aws-sdk/clients/dynamodb'
import Logger from './logger'

interface MyData {
  [id: string]: MyData | string | number | string[] | number[] | MyData[]
}
var awsConfig: AWS.DynamoDB.Types.ClientConfiguration = {}
if(config.aws?.region) {
  awsConfig['region'] = config.aws?.region;
}
if(config.aws?.credentials) {
  awsConfig['credentials'] = config.aws?.credentials;
}
const cl = new AWS.DynamoDB(awsConfig)
module.exports = function (data: MyData) {
  const tableToChange: {[id: string]: WriteRequests} = {}
  let handle = 'unknown'
  if(typeof data.handle === 'string') {
    handle = data.handle
  }
  tableToChange[handle] = [{}]
  const params: AWS.DynamoDB.BatchWriteItemInput = {
    RequestItems: tableToChange
  }
  cl.batchWriteItem(params, (err: AWS.AWSError, data: AWS.DynamoDB.BatchWriteItemOutput) => {
    if(err) {
      Logger.warn(err)
    }
  });
  Logger.info('Submit Data', data);
}
