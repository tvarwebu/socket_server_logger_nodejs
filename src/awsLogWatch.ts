import AWS from 'aws-sdk'
import * as _config from './config.json'
import Config from './interfaces/Config';
import { MetricDatum, Dimension } from 'aws-sdk/clients/cloudwatch';
import Logger from './logger'

const config: Config = _config

const awsSubmitDataTimeout = config.aws?.cloudWatch?.pushSeconds ?? 60;
const awsGroupDataTimeout = 1;
var awsConfig: AWS.CloudWatch.Types.ClientConfiguration = {}
if(config.aws?.region) {
  awsConfig['region'] = config.aws?.region;
}
if(config.aws?.credentials) {
  awsConfig['credentials'] = config.aws?.credentials;
}
const cw = new AWS.CloudWatch(awsConfig)
var timer: NodeJS.Timeout | undefined
var timerGroup: NodeJS.Timeout | undefined
var cachedData: MetricDatum[] = []
var cachedSecond: {[id: string]: number} = {}
function submitData() {
  const cached = cachedData;
  timer = undefined
  if(config.aws?.cloudWatch?.namespace) {
    for(let i = 0; i < cached.length / 20; i++) {
      cw.putMetricData({
        Namespace: config.aws?.cloudWatch?.namespace,
        MetricData: cached.slice(i * 20, (i + 1) * 20),
      }, function(err, data) {
        if (err) {
          Logger.warn("aws push failed", err, err.stack); // an error occurred
        }
        Logger.info("awsLogWatch ["+((new Date()).toISOString())+"] "+(err ? err : "OK"))
      })
    }
  } else {
    Logger.warn("Missing CloudWatch namespace configuration")
  }
  cachedData = []
}
export default function awsLogWatch (handle: string) {
  if(!cachedSecond[handle]) {
    cachedSecond[handle] = 0;
  }
  cachedSecond[handle] += 1;
  if(!timerGroup) {
    timerGroup = setTimeout(groupData, 1000 * awsGroupDataTimeout)
  }
}
function groupData() {
  timerGroup = undefined
  var dimensions: Dimension[] | undefined = undefined
  if(config.aws?.cloudWatch?.dimensions) {
    dimensions = config.aws.cloudWatch.dimensions
  }
  const cachedS = cachedSecond
  for (const handle in cachedS) {
    if (cachedSecond.hasOwnProperty(handle)) {
      cachedData.push({
        MetricName: handle,
        Timestamp: new Date(),
        Dimensions: dimensions,
        Value: cachedSecond[handle],
        Unit: "Count"
      })
    }
  }
  cachedSecond = {}
  if(!timer) {
    timer = setTimeout(submitData, 1000 * awsSubmitDataTimeout)
  }
}
